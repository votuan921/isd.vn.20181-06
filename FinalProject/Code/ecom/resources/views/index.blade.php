@extends('layouts.frontLayout.front_design')

@section('content')

<section id="slider"><!--slider-->
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							@foreach($banners as $key => $banner)
								<li data-target="#slider-carousel" data-slide-to="0" @if($key==0) class="active" @endif></li>
							@endforeach
						</ol>
						
						<div class="carousel-inner">
							@foreach($banners as $key => $banner)
							<div class="item @if($key==0) active @endif">
								<a href="{{ $banner->link }}" title="Banner 1"><img src="images/frontend_images/banners/{{ $banner->image }}"></a>
							</div>
							@endforeach
						</div>
						
						<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
							<i class="fa fa-angle-left"></i>
						</a>
						<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
							<i class="fa fa-angle-right"></i>
						</a>
					</div>
					
				</div>
			</div>
		</div>
</section><!--/slider-->
	
<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					@include('layouts.frontLayout.front_sidebar')
				</div>
				
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Features Items</h2>
						@if(count($productsAll) == 0) <h3 class="title text-center">Không tìm thấy sản phẩm phù hợp</h3> @endif
						@foreach($productsAll as $pro)
						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
										<div class="productinfo text-center">
											<img src="{{ asset('/images/backend_images/product/small/'.$pro->image) }}" alt="" />
											<h2>$ {{ $pro->price }}</h2>
											<p>{{ $pro->product_name }}</p>
											<a href="{{ url('/product/'.$pro->id) }}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>View Product</a>
										</div>
										<div class="product-overlay">
											<div class="overlay-content">
												<h2>$ {{ $pro->price }}</h2>
												<p>{{ $pro->product_name }}</p>
												<a href="{{ url('/product/'.$pro->id) }}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>View Product</a>
											</div>
										</div>
								</div>
								<!-- <div class="choose">
									<ul class="nav nav-pills nav-justified">
										<li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
										<li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
									</ul>
								</div> -->
							</div>
						</div>
						@endforeach
						
						
					</div><!--features_items-->
					@if(count($productsAll)>3)
					<div class="recommended_items"><!--recommended_items-->
						<h2 class="title text-center">recommended items</h2>
						
						<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner">
								<div class="item active">
									
									@for ($i = 0; $i < 3; $i++)
										<div class="col-sm-4">
											<div class="product-image-wrapper">
												<div class="single-products">
													<div class="productinfo text-center">
														<img src="{{ asset('/images/backend_images/product/small/'.$productsAll[$i]->image) }}" alt="" style="width: 200px; height: 200px"/>
														<h2>$ {{ $productsAll[$i]->price }}</h2>
														<p>{{ $productsAll[$i]->product_name }}</p>
														<a href="{{ url('/product/'.$productsAll[$i]->id) }}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>View Product</a>
													</div>
												</div>
											</div>
										</div>
									@endfor
								</div>
								@if(count($productsAll)>5)
								<div class="item">	
									@for ($i = 3; $i < 6; $i++)
										<div class="col-sm-4">
											<div class="product-image-wrapper">
												<div class="single-products">
													<div class="productinfo text-center">
														<img src="{{ asset('/images/backend_images/product/small/'.$productsAll[$i]->image) }}" alt="" style="width: 200px; height: 200px"/>
														<h2>$ {{ $productsAll[$i]->price }}</h2>
														<p>{{ $productsAll[$i]->product_name }}</p>
														<a href="{{ url('/product/'.$productsAll[$i]->id) }}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>View Product</a>
													</div>
												</div>
											</div>
										</div>
									@endfor
								</div>
								@endif
							</div>
							 <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
								<i class="fa fa-angle-left"></i>
							  </a>
							  <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
								<i class="fa fa-angle-right"></i>
							  </a>			
						</div>
					</div><!--/recommended_items-->
					@endif
				</div>
			</div>
		</div>
</section>

@endsection