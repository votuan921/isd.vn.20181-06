<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use Session;
use Image;
use App\Category;
use App\Product;
use App\ProductsAttribute;
use App\ProductsImage;
use App\Coupon;
use App\User;
use App\Country;
use App\DeliveryAddress;
use App\Order;
use App\OrdersProduct;
use App\ReviewsComment;
use App\ReviewsImage;
use DB;

class OrdersController extends Controller
{
    public function orderReview() {
        $user_id = Auth::user()->id;
        $user_email = Auth::user()->email;
        $userDetails = User::where('id', $user_id)->first();
        $shippingDetails = DeliveryAddress::where('user_id', $user_id)->first();
        $shippingDetails = json_decode(json_encode($shippingDetails));

        $userCart = DB::table('cart')->where(['user_email' => $user_email])->get();
        foreach($userCart as $key => $product){
            $productDetails = Product::where('id',$product->product_id)->first();
            $userCart[$key]->image = $productDetails->image;
        }
        return view('products.order_review')->with(compact('userDetails', 'shippingDetails', 'userCart'));
    }

    public function placeOrder(Request $request) {
        if($request->isMethod('post')) {
            $data = $request->all();
            $user_id = Auth::user()->id;
            $user_email = Auth::user()->email;

            $shippingDetails = DeliveryAddress::where(['user_email'=>$user_email])->first();

            if(empty(Session::get('CouponCode'))) {
                $coupon_code = '';
            }
            else {
                $coupon_code = Session::get('CouponCode');
            }
            if(empty(Session::get('CouponAmount'))) {
                $coupon_amount = '';
            }
            else {
                $coupon_amount = Session::get('CouponAmount');
            }
            $order = new Order;
            $order->user_id = $user_id;
            $order->user_email = $user_email;
            $order->name = $shippingDetails->name;
            $order->address = $shippingDetails->address;
            $order->city = $shippingDetails->city;
            $order->state = $shippingDetails->state;
            $order->country = $shippingDetails->country;
            $order->pincode = $shippingDetails->pincode;
            $order->mobile = $shippingDetails->mobile;
            $order->coupon_code = $coupon_code;
            $order->coupon_amount = $coupon_amount;
            $order->order_status = "New";
            $order->payment_method = $data['payment_method'];
            $order->grand_total = $data['grand_total'];
            $order->save();

            $order_id = DB::getPdo()->lastInsertId();

            $cartProducts = DB::table('cart')->where(['user_email'=>$user_email])->get();
            foreach ($cartProducts as $pro) {
                $cartPro = new OrdersProduct;
                $cartPro->order_id = $order_id;
                $cartPro->user_id = $user_id;
                $cartPro->product_id = $pro->product_id;
                $cartPro->product_code = $pro->product_code;
                $cartPro->restaurant_id = $pro->restaurant_id;
                $cartPro->product_name = $pro->product_name;
                $cartPro->product_color = $pro->product_color;
                $cartPro->product_size = $pro->size;
                $cartPro->product_price = $pro->price;
                $cartPro->product_qty = $pro->quantity;
                $cartPro->save();
            }

            Session::put('order_id', $order_id);
            Session::put('grand_total', $data['grand_total']);
            if ($data['payment_method'] == "COD") {
                return redirect('/thanks');
            }
            else {
                return redirect('/paypal');
            }
            

            // if ($data['payment_method'] == "COD") {
            //     return redirect()->back()->with('flash_message_success','Your order has been successfully sent to the shop');
            // }
            // else {
            //     echo "<prep>";
            //     print_r($data);
            //     die;
            // }
        }
    }

    public function thanks() {
        $user_email = Auth::user()->email;
        DB::table('cart')->where('user_email', $user_email)->delete();
        Session::forget('CouponAmount');
        Session::forget('CouponCode');
        return view('orders.thanks');
    }

    public function paypal() {
        $user_email = Auth::user()->email;
        DB::table('cart')->where('user_email', $user_email)->delete();
        Session::forget('CouponAmount');
        Session::forget('CouponCode');
        return view('orders.paypal');
    }


    public function userOrders() {
        $user_id = Auth::user()->id;
        $orders = Order::with('orders')->where('user_id', $user_id)->orderBy('id', 'DESC')->get();
        return view('orders.user_orders')->with(compact('orders'));
    }

    public function userOrderDetails($order_id) {
        $user_id = Auth::user()->id;
        $orderDetails = Order::with('orders')->where('user_id', $user_id)->first();
        $orderDetails = json_decode(json_encode($orderDetails));
        return view('orders.user_order_details')->with(compact('orderDetails'));
    }

    public function viewOrders() {
        $orders =  Order::with('orders')->orderBy('id', 'DESC')->get();
        $orders = json_decode(json_encode($orders));
        return view('admin.orders.view_orders')->with(compact('orders'));
    }

    public function viewOrderDetails($order_id) {

        $orderDetails = Order::with('orders')->where('id', $order_id)->first();
        $orderDetails = json_decode(json_encode($orderDetails));
        $user_id = $orderDetails->user_id;
        $userDetails = User::where('id', $user_id)->first();
        $shippingDetails = DeliveryAddress::where('user_id', $user_id)->first();
        return view('admin.orders.order_details')->with(compact('orderDetails', 'userDetails', 'shippingDetails'));
    }

    public function updateStatus(Request $request) {
        if($request->isMethod('post')) {
            $data = $request->all();
            $order_id = $data['order_id'];
            Order::where('id', $order_id)->update(['order_status'=>$data['update_status']]);
        }
        $orders =  Order::with('orders')->orderBy('id', 'DESC')->get();
        return redirect()->back()->with(compact('orders'));
    }
}
