<?php
namespace App\Http\Middleware;
use Illuminate\Http\Request;
use Fideloper\Proxy\TrustProxies as Middleware;
class TrustProxies extends Middleware
{
    protected $proxies = [
        '10.138.235.208'
    ];
    protected $headers = Request:: HEADER_X_FORWARDED_AWS_ELB;
}